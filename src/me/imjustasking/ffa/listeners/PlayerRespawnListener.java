package me.imjustasking.ffa.listeners;

import me.imjustasking.ffa.managers.KitManager;
import me.imjustasking.ffa.managers.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener implements Listener {

    private KitManager kitManager = new KitManager();
    private ScoreboardManager scoreboardManager = new ScoreboardManager();

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setExp(0);
        player.setFlying(false);

        kitManager.setKit(player);
    }
}

package me.imjustasking.ffa.listeners;

import me.imjustasking.ffa.Core;
import me.imjustasking.ffa.managers.ConfigManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class BlockRunnableListeners implements Listener {

    private ConfigManager configManager = new ConfigManager();

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();

        if((!player.hasPermission("rank.owner")) || (!player.isOp()) || (!player.hasPermission("rank.builder"))) {
            event.setCancelled(true);
            player.sendMessage(configManager.getCantBreakBlocksMsg());
        }
    }

    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent event) {
        Material material = event.getBlock().getType();

        if(material == Material.WOOD) {
            new BukkitRunnable() {
                public void run() {
                    ItemStack wood = new ItemStack(Material.WOOD, 1);

                    event.getBlock().setType(Material.AIR);
                    event.getPlayer().getInventory().addItem(wood);
                }
            }.runTaskLater(Core.plugin, 100);

        } else if(material == Material.COBBLESTONE) {
            new BukkitRunnable() {
                public void run() {
                    ItemStack cobblestone = new ItemStack(Material.COBBLESTONE, 1);

                    event.getBlock().setType(Material.AIR);
                    event.getPlayer().getInventory().addItem(cobblestone);
                }
            }.runTaskLater(Core.plugin, 100);

        } else if(event.getItemInHand().getType().equals(Material.FLINT_AND_STEEL)) {
            new BukkitRunnable() {
                public void run() {
                    if (event.getBlock().getType().equals(Material.FIRE)) {
                        event.getBlock().setType(Material.AIR);
                    }
                }
            }.runTaskLater(Core.plugin, 50);
        }
    }

    @EventHandler
    public void onBlockFromTo(BlockFromToEvent event) {

        if((event.getBlock().getType().equals(Material.WATER)) || (event.getBlock().getType().equals(Material.STATIONARY_WATER))) {
            event.setCancelled(true);
        }
    }
}

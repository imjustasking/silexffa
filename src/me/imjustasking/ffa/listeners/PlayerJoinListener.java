package me.imjustasking.ffa.listeners;

import me.imjustasking.ffa.managers.ConfigManager;
import me.imjustasking.ffa.managers.KitManager;
import me.imjustasking.ffa.managers.ScoreboardManager;
import me.imjustasking.ffa.scoreboard.FFAScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    private ConfigManager configManager = new ConfigManager();
    private KitManager kitManager = new KitManager();
    private ScoreboardManager scoreboardManager = new ScoreboardManager();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        World spawn_world = Bukkit.getWorld(configManager.getSpawnWorldName());

        event.setJoinMessage(null);
        player.teleport(new Location(spawn_world, configManager.getSpawnLocationX(), configManager.getSpawnLocationY(), configManager.getSpawnLocationZ()));
        new FFAScoreboard().setScoreboard(player);

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setExp(0);
        player.setFlying(false);

        kitManager.setKit(player);

        if(Bukkit.getOnlinePlayers().length == 1) {

        }
    }
}

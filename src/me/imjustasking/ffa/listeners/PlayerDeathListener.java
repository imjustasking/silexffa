package me.imjustasking.ffa.listeners;

import me.imjustasking.ffa.managers.ConfigManager;
import me.imjustasking.ffa.managers.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerDeathListener implements Listener {

    private ConfigManager configManager = new ConfigManager();

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        if(event.getEntity() instanceof Player && event.getEntity().getKiller() instanceof Player) {
            Player player = event.getEntity().getPlayer();
            Player killer = event.getEntity().getKiller();

            event.setDeathMessage(null);
            event.getDrops().clear();

            ItemStack reward = new ItemStack(Material.GOLDEN_APPLE, 3);
            killer.getInventory().addItem(reward);

            killer.sendMessage(configManager.getKillMsg(player));
            player.sendMessage(configManager.getKilledByPlayerMsg(killer));

        } else {
            event.setDeathMessage(null);
            event.getDrops().clear();
        }
    }
}
package me.imjustasking.ffa.scoreboard;

import me.imjustasking.ffa.managers.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scoreboard.*;

public class FFAScoreboard implements Listener {

    private ConfigManager configManager = new ConfigManager();
    private me.imjustasking.ffa.managers.ScoreboardManager scoreboardManager = new me.imjustasking.ffa.managers.ScoreboardManager();

    public void setScoreboard(Player player) {
        if(player != null) {
            ScoreboardManager scoreboardManager = Bukkit.getScoreboardManager();
            Scoreboard scoreboard = scoreboardManager.getNewScoreboard();

            Objective objective = scoreboard.registerNewObjective("ffaboard", "dummy");
            Objective playerHealth = scoreboard.registerNewObjective("player", "health");

            objective.setDisplayName(configManager.getBoardTitle());
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);

            playerHealth.setDisplayName("§4❤");
            playerHealth.setDisplaySlot(DisplaySlot.BELOW_NAME);

            Team team1 = scoreboard.registerNewTeam(ChatColor.AQUA.toString());

            if(configManager.getBoardSeparatorsStatus() == true) {
                team1.addEntry("§7§m--------");
                team1.setSuffix("------------");
                objective.getScore("§7§m--------").setScore(10);

            } else {
                team1.addEntry("§1");
                objective.getScore("§1").setScore(10);
            }

            Team team2 = scoreboard.registerNewTeam(ChatColor.DARK_AQUA.toString());
            team2.addEntry("§6Play");
            team2.setSuffix("er:");
            objective.getScore("§6Play").setScore(9);

            Team team3 = scoreboard.registerNewTeam(ChatColor.RED.toString());
            team3.addEntry(ChatColor.RED + player.getName());
            objective.getScore(ChatColor.RED + player.getName()).setScore(8);

            Team team4 = scoreboard.registerNewTeam(ChatColor.DARK_RED.toString());
            team4.addEntry("§2");
            objective.getScore("§2").setScore(7);

            Team team5 = scoreboard.registerNewTeam(ChatColor.GREEN.toString());
            team5.addEntry("§6Kills: §c" + player.getStatistic(Statistic.PLAYER_KILLS));
            objective.getScore("§6Kills: §c" + player.getStatistic(Statistic.PLAYER_KILLS)).setScore(6);

            Team team6 = scoreboard.registerNewTeam(ChatColor.DARK_GREEN.toString());
            team6.addEntry("§6Deaths: §c" + player.getStatistic(Statistic.DEATHS));
            objective.getScore("§6Deaths: §c" + player.getStatistic(Statistic.DEATHS)).setScore(5);

            Team team7 = scoreboard.registerNewTeam(ChatColor.DARK_PURPLE.toString());
            team7.addEntry("§3");
            objective.getScore("§3").setScore(4);

            Team team8 = scoreboard.registerNewTeam(ChatColor.LIGHT_PURPLE.toString());
            team8.addEntry("§6Region: §c");
            team8.setSuffix("North America");
            objective.getScore("§6Region: §c").setScore(3);

            Team team9 = scoreboard.registerNewTeam(ChatColor.WHITE.toString());
            team9.addEntry("§6Onl");
            team9.setSuffix("ine: " + ChatColor.RED + Bukkit.getOnlinePlayers().length + "/" + Bukkit.getMaxPlayers());
            objective.getScore("§6Onl").setScore(2);

            Team team10 = scoreboard.registerNewTeam(ChatColor.BLACK.toString());
            if(configManager.getBoardSeparatorsStatus() == true) {
                team10.addEntry("§7§m------");
                team10.setSuffix("--------------");
                objective.getScore("§7§m------").setScore(1);

            } else {
                team10.addEntry("§4");
                objective.getScore("§4").setScore(1);
            }

            player.setScoreboard(scoreboard);

        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        setScoreboard(player);
        for(Player all : Bukkit.getOnlinePlayers()) {
            scoreboardManager.updateScoreboard(player);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        setScoreboard(player);
        for(Player all : Bukkit.getOnlinePlayers()) {
            scoreboardManager.updateScoreboard(player);
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        setScoreboard(player);
        for(Player all : Bukkit.getOnlinePlayers()) {
            scoreboardManager.updateScoreboard(player);
        }
    }
}
package me.imjustasking.ffa.managers;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class KitManager {

    public void setKit(Player player) {
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
        ItemStack rod = new ItemStack(Material.FISHING_ROD);
        ItemStack bow = new ItemStack(Material.BOW);
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
        ItemStack arrows = new ItemStack(Material.ARROW, 64);
        ItemStack fns = new ItemStack(Material.FLINT_AND_STEEL, 1, (short) 2);
        ItemStack water = new ItemStack(Material.WATER_BUCKET);
        ItemStack goldenapple = new ItemStack(Material.GOLDEN_APPLE, 3);
        ItemStack food = new ItemStack(Material.COOKED_BEEF, 16);
        ItemStack wood = new ItemStack(Material.WOOD, 32);
        ItemStack stone = new ItemStack(Material.COBBLESTONE, 32);

        ItemStack armor1 = new ItemStack(Material.DIAMOND_HELMET);
        armor1.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        ItemStack armor2 = new ItemStack(Material.DIAMOND_CHESTPLATE);
        armor2.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        ItemStack armor3 = new ItemStack(Material.DIAMOND_LEGGINGS);
        armor3.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        ItemStack armor4 = new ItemStack(Material.DIAMOND_BOOTS);
        armor4.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);

        player.getInventory().addItem(sword,rod,bow,fns,water,goldenapple,food,wood,stone,arrows);
        player.getInventory().setHelmet(armor1);
        player.getInventory().setChestplate(armor2);
        player.getInventory().setLeggings(armor3);
        player.getInventory().setBoots(armor4);
    }
}

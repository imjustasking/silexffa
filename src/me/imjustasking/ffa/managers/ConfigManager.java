package me.imjustasking.ffa.managers;

import me.imjustasking.ffa.Core;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;

public class ConfigManager {

    FileConfiguration configuration = Core.plugin.getConfig();

    // GET PREFIXES //

    public String getMainPrefix() {
        return configuration.getString("prefixes.main-prefix").replace(
                "&", "§"
        );
    }

    public String getErrorPrefix() {
        return configuration.getString("prefixes.error-prefix").replace(
                "&", "§"
        );
    }

    // GET MESSAGES //

    public String getKilledByPlayerMsg(Player killer) {
        double health = ((Damageable)killer).getHealth();

        return configuration.getString("messages.killed-by-player-msg").replace(
                "&", "§").replace("%player%", killer.getName()).replace("%player_health%", String.valueOf(Math.round(health))).replace("{heart}", "❤");
    }

    public String getKillMsg(Player player) {
        return configuration.getString("messages.kill-msg").replace(
                "&", "§").replace("%player%", player.getName());
    }

    public String getCantBreakBlocksMsg() {
        return configuration.getString("messages.cant-break-blocks-msg");
    }

    // GET SCOREBOARD SETTINGS //

    public String getBoardTitle() {
        return configuration.getString("scoreboard.board-title").replace(
                "&", "§");
    }

    public boolean getBoardSeparatorsStatus() {
        return configuration.getBoolean("scoreboard.board-separators");
    }

    // GET EXTRAS //

    public String getSpawnWorldName() {
        return configuration.getString("extras.spawn-world-name");
    }

    public Integer getSpawnLocationX() {
        return configuration.getInt("extras.spawn-location.x");
    }

    public Integer getSpawnLocationY() {
        return configuration.getInt("extras.spawn-location.y");
    }

    public Integer getSpawnLocationZ() {
        return configuration.getInt("extras.spawn-location.z");
    }
}

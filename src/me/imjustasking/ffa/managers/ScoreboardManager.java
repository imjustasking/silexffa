package me.imjustasking.ffa.managers;

import me.imjustasking.ffa.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardManager {

    public void updateScoreboard(Player player) {
        Scoreboard board = player.getScoreboard();

        Team update1 = board.getTeam(ChatColor.GREEN.toString());
        update1.addEntry("§6Kills: §c" + player.getStatistic(Statistic.PLAYER_KILLS));

        Team update2 = board.getTeam(ChatColor.DARK_GREEN.toString());
        update2.addEntry("§6Deaths: §c" + player.getStatistic(Statistic.DEATHS));

        Team update3 = board.getTeam(ChatColor.WHITE.toString());
        update3.setSuffix("ine: " + ChatColor.RED + Bukkit.getOnlinePlayers().length + "/" + Bukkit.getMaxPlayers());
    }
}

package me.imjustasking.ffa;

import me.imjustasking.ffa.listeners.*;
import me.imjustasking.ffa.managers.ScoreboardManager;
import me.imjustasking.ffa.scoreboard.FFAScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin implements Listener {

    public static Core plugin;

    FileConfiguration configuration = getConfig();

    @Override
    public void onEnable() {
        World spawn_world = Bukkit.getWorld(configuration.getString("extras.spawn-world-name"));

        if(!(spawn_world == null)) {
            plugin = this;

            loadListeners();
            loadConfig();

            Bukkit.getConsoleSender().sendMessage("§8§m----------------------------------------");
            Bukkit.getConsoleSender().sendMessage("");
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_GREEN + "Succesfully enabled in " + Bukkit.getServer().getIp() + "!");
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_GREEN + "Coded by @_ImJustAsking_.");
            Bukkit.getConsoleSender().sendMessage("");
            Bukkit.getConsoleSender().sendMessage("§8§m----------------------------------------");

        } else {
            Bukkit.getConsoleSender().sendMessage("§8§m----------------------------------------");
            Bukkit.getConsoleSender().sendMessage("");
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Couldn't enable plugin in " + Bukkit.getServer().getIp() + "!");
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Spawn world has not been created yet!");
            Bukkit.getConsoleSender().sendMessage("");
            Bukkit.getConsoleSender().sendMessage("§8§m----------------------------------------");
        }
    }

    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "SilexFFA plugin succesfully disabled.");
    }

    private void loadListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new PlayerJoinListener(), this);
        pluginManager.registerEvents(new PlayerQuitListener(), this);
        pluginManager.registerEvents(new PlayerDeathListener(), this);
        pluginManager.registerEvents(new PlayerRespawnListener(), this);
        pluginManager.registerEvents(new BlockRunnableListeners(), this);
        pluginManager.registerEvents(new FFAScoreboard(), this);
    }

    private void loadConfig() {
        if(configuration.getKeys(false).isEmpty()) {
            saveDefaultConfig();
        }
    }
}